﻿

#include <iostream>


class Animal
{
public:
	virtual void Voice() const = 0; // виртуальная функция

	virtual ~Animal() {}//ВИРТУФЛЬНЫЙ ДЕСТРУКТОР
};

class Dog : public Animal
{
	void Voice() const override
	{
		std::cout << "Dog-Woof" << std::endl;
	}
};
class Cat : public Animal
{
	void Voice() const override
	{
		std::cout << "Cat-Myyyyuuuu" << std::endl;
	}

};
class Wolf : public Animal
{
	void Voice() const override
	{
		std::cout << "Wolf-RRRRRRRR" << std::endl;
	}


};


int main()
{
	const int animalCount = 3;

	Animal* animals[animalCount];
	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Wolf();

	for (int i = 0; i < animalCount; i++)
	{
		animals[i]->Voice();
	}

	for (int i = 0; i < animalCount; ++i)
	{
		delete animals[i];
	}

	return 0;
}
